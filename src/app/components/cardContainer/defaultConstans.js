import first1 from '../../../assets/images/homePage/first1.jpg'
import first2 from '../../../assets/images/homePage/first2.jpg'
import second1 from '../../../assets/images/homePage/second1.jpg'
import second2 from '../../../assets/images/homePage/second2.jpg'
import second3 from '../../../assets/images/homePage/second3.jpg'
import second4 from '../../../assets/images/homePage/second4.jpg'



export const firstSlide = [
    { img: first1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: first2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
] 
export const secondSlide = [
    { img: second1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: second2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second3, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second4, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
] 
export const thirdSlide = [
    { img: second1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: second2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: first1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: first2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second3, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second4, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
] 
export const forthSlide = [
    { img: second1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: second2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: first1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: first2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second3, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: first1, titleId: 'T-Bone Steak & Eggs', descriptionId1: "A'la Carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "4-6 Mins",detail2: "$16.99" },
    { img: first2, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
    { img: second4, titleId: 'Eggs Benedict', descriptionId1: "à la carte", descriptionId2: "American", descriptionId3: "Appetizer", detail1: "5-7 Mins",detail2: "$12.99" },
] 