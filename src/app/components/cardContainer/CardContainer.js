import React, { useState } from 'react'
import './CardContainer.scss'
import EachCard from '../eachCard/EachCard'
import { Row, Col } from 'antd'
import rightArrow from '../../../assets/images/homePage/rightArrow.svg'
import { firstSlide, secondSlide, thirdSlide, forthSlide } from '../cardContainer/defaultConstans'


const CardContainer = (props) => {
    const [percant, setPercant] = useState("0%")
    const [transforms, setTransforms] = useState(0)
    const rightClicked = () => {
        setPercant("100%")
        if (props.section === 4 && transforms > -200) {
            setTransforms(transforms - 100)
        }
        else if (transforms > -100) {
            setTransforms(transforms - 100)
        }
    }
    const leftClicked = () => {
        setPercant("0%")
        if (transforms < 0) {
            setTransforms(transforms + 100)
        }
    }
    const showFirstSlide = () => {

        switch (props.section) {
            case 1:
                return (
                    firstSlide.map((item, index) =>
                        <Col xxl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} xl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} lg={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} md={{ pull: 1, offset: `${index === 0 ? 1 : 0}`, span: 12 }} sm={{ offset: `${index === 0 ? 1 : 0}`, span: 12 }} xs={{ offset: `${index === 0 ? 1 : 0}`, span: 18 }}
                            className="components-CardContainer-col">
                            <EachCard
                                img={item.img}
                                titleId={item.titleId}
                                descriptionId1={item.descriptionId1}
                                detailId1={item.detail1}
                                detailId2={item.detail2}
                                descriptionId2={item.descriptionId2}
                                descriptionId3={item.descriptionId3} />
                        </Col>)
                )
            case 2:
                return (
                    secondSlide.map((item, index) =>
                        <Col xxl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} xl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} lg={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} md={{ pull: 1, offset: `${index === 0 ? 1 : 0}`, span: 12 }} sm={{ offset: `${index === 0 ? 1 : 0}`, span: 12 }} xs={{ offset: `${index === 0 ? 1 : 0}`, span: 18 }}
                            className="components-CardContainer-col">
                            <EachCard
                                img={item.img}
                                titleId={item.titleId}
                                descriptionId1={item.descriptionId1}
                                detailId1={item.detail1}
                                detailId2={item.detail2}
                                descriptionId2={item.descriptionId2}
                                descriptionId3={item.descriptionId3} />
                        </Col>)
                )
            case 3:
                return (
                    thirdSlide.map((item, index) =>
                        <Col xxl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} xl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} lg={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} md={{ pull: 1, offset: `${index === 0 ? 1 : 0}`, span: 12 }} sm={{ offset: `${index === 0 ? 1 : 0}`, span: 12 }} xs={{ offset: `${index === 0 ? 1 : 0}`, span: 18 }}
                            className="components-CardContainer-col">
                            <EachCard
                                img={item.img}
                                titleId={item.titleId}
                                descriptionId1={item.descriptionId1}
                                detailId1={item.detail1}
                                detailId2={item.detail2}
                                descriptionId2={item.descriptionId2}
                                descriptionId3={item.descriptionId3} />
                        </Col>)
                )
            case 4:
                return (
                    forthSlide.map((item, index) =>
                        <Col xxl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} xl={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} lg={{ offset: `${index === 0 ? 1 : 0}`, span: 7 }} md={{ pull: 1, offset: `${index === 0 ? 1 : 0}`, span: 12 }} sm={{ offset: `${index === 0 ? 1 : 0}`, span: 12 }} xs={{ offset: `${index === 0 ? 1 : 0}`, span: 18 }}
                            className="components-CardContainer-col">
                            <EachCard
                                img={item.img}
                                titleId={item.titleId}
                                descriptionId1={item.descriptionId1}
                                detailId1={item.detail1}
                                detailId2={item.detail2}
                                descriptionId2={item.descriptionId2}
                                descriptionId3={item.descriptionId3} />
                        </Col>)
                )

            default:
                break;
        }
    }
    return (
        <div className="components-CardContainer-main">
            <p className="components-CardContainer-title">Special Offers</p>
            <div className="components-CardContainer-horizental-scroll">
                <div style={{ left: percant }} className="components-CardContainer-horizental-scroll__cursor"></div>
            </div>
            <div className="components-CardContainer-crausel">
                <Row justify="start" className="components-CardContainer-row" style={{ transform: `translateX(${transforms}%)` }}>
                    {showFirstSlide()}
                </Row>
            </div>
            <button style={props.section === 1 ? { display: "none" } : { display: "block" }} onClick={rightClicked} className="components-CardContainer-button">
                <img src={rightArrow} />
            </button>
            <button style={props.section === 1 ? { display: "none" } : { display: "block" }} onClick={leftClicked} className="components-CardContainer-button__left">
                <img src={rightArrow} />
            </button>
        </div>
    )
}

export default CardContainer
