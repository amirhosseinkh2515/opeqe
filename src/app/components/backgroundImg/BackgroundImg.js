import React, { Component } from 'react'
import './BackgroundImg.scss'
import backgroundImg from '../../../assets/images/homePage/backgroundImg.jpg'

export class BackgroundImg extends Component {
    render() {
        return (
            <div className="components-backgroundImg-main">
                <div className="components-backgroundImg-main__img-container">
                    <img className="components-backgroundImg-main__img" src={backgroundImg} />
                </div>
                <div className="components-backgroundImg-main__title-container">
                    <div>
                        <p className="components-backgroundImg-main__title-header">Chief Special</p>
                        <p className="components-backgroundImg-main__title-description">Get $10 off when you order $20 or more T-Bone Steak & Eggs</p>
                    </div>
                    <div className="components-backgroundImg-main__button-container">
                        <p> Use </p>
                        <span> Chief Special </span>
                    </div>
                </div>
            </div>
        )
    }
}

export default BackgroundImg
