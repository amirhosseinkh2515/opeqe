import React, { Component } from 'react'
import './Header.scss'
import logo from '../../../assets/images/homePage/logo.svg'
import bag from '../../../assets/images/homePage/bag.svg'


const Header = () => {
    return (
        <div
            style={{ backgroundColor: "rgb(239, 239, 239)" }}
            className="components-header-main"
        >
            <div className="components-header-main__container">
                <div className="components-header-main__left">
                    <img src={logo} />
                </div>
                <div className="components-header-main__right">
                    <a href="/" className="components-header-main__right-a">
                        Reservation
                    </a>
                    <a href="/" className="components-header-main__right-a">
                        Orders
                    </a>
                    <a href="/" className="components-header-main__right-a">
                        Locations
                    </a>
                    <a href="/" className="components-header-main__right-sign-in">
                        Log In
                    </a>
                    <a href="/" className="components-header-main__right-sign-up">
                        Sign Up
                    </a>
                    <a href="/" className="components-header-main__right-bag">
                        <img className="components-header-main__right-bag-img" src={bag}/>
                    </a>
                </div>
            </div>

        </div>
    )
}

export default Header

