import React, { Component } from 'react'
import './EachCard.scss'
import clock from '../../../assets/images/homePage/clock.svg'


const EachCard = (props) => {
    return (
        <section className='components-eachcard-main'>
            <div className='components-eachcard-main__img-container'>
                <img className='components-eachcard-main__img' src={props.img} />
                <p>Chief Special</p>
            </div>
            <div className='components-eachcard-title'>
                {/* <p>{props.titleId}</p> */}
                <p>{props.titleId}</p>
            </div>
            <div className='components-eachcard-description'>
                {/* <span >{props.descriptionId}</span> */}
                <span className='components-eachcard-description__first'>{props.descriptionId1}</span>.
                <span className='components-eachcard-description__second'>{props.descriptionId2} </span>.
                <span className='components-eachcard-description__second'>{props.descriptionId3}</span>
            </div>
            <div className='components-eachcard-detail'>
                {/* <p>{props.detailId}</p> */}
                <img src={clock} />
                <spna className='components-eachcard-detail__time'>{props.detailId1}</spna>
                <spna className='components-eachcard-detail__price'>{props.detailId2}</spna>
            </div>
            <p className='components-eachcard-free'>Free Pickup</p>
        </section>
    )
}

export default EachCard
