import React, { useState } from 'react'
import './Stickynavbar.scss'

const Stickynavbar = () => {
    const [condition, setCondition] = useState("delivery")
    const [width, setWidth] = useState(50)
    const [translateX, setTranslateX] = useState("translateX(0)")

    const handleDelvery = () => {
        setCondition("delivery")
        setWidth(50)
        setTranslateX("translateX(0)")
    }
    const handlePickUp = () => {
        setCondition("pickup")
        setWidth(42)
        setTranslateX("translateX(85px)")
    }
    const conditionChanger = (condition) => {
        switch (condition) {
            case "delivery":
                return (
                    <div className="components-stickynavbar-main__container-left-left">
                        <p className="components-stickynavbar-main__container-left-left-ASAP">ASAP Delivery</p>
                        <p className="components-stickynavbar-main__container-left-left-noaddress">What's Your Address ?</p>
                    </div>
                )
            case "pickup":
                return (
                    <div className="components-stickynavbar-main__container-left-left">
                        <p className="components-stickynavbar-main__container-left-left-ASAP">ASAP Pickup</p>
                        <p className="components-stickynavbar-main__container-left-left-address">Beverly Hills - 1008 Elden Way</p>
                    </div>
                )
            default:
                break;
        }
    }
    return (
        <div className="components-stickynavbar-main">
            <div className="components-stickynavbar-main__container">
                <div className="components-stickynavbar-main__container-left">
                    {conditionChanger(condition)}
                    <p className="components-stickynavbar-main__container-left-middle"> Change </p>
                    <div className="components-stickynavbar-main__container-left-right">
                        <div style={{width: width, transform: translateX}}></div>
                        <p onClick={handleDelvery}> Delivery </p>
                        <span> or </span>
                        <p onClick={handlePickUp}> Pickup </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Stickynavbar
