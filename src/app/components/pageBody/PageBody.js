import React, { Component } from 'react'
import "./PageBody.scss"
import Stickynavbar from '../stickynavbar/Stickynavbar'
import CardContainer from '../cardContainer/CardContainer'


export class PageBody extends Component {
    render() {
        return (
            <div className="components-pageBody-main">
                <Stickynavbar />
                <CardContainer section={1}/>
                <CardContainer section={2}/>
                <CardContainer section={3}/>
                <CardContainer section={4}/>


            </div>
        )
    }
}

export default PageBody
