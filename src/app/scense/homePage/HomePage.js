import React, { Component } from 'react'
import './HomePage.scss'
import Header from '../../components/header/Header'
import BackgroundImg from '../../components/backgroundImg/BackgroundImg'
import PageBody from '../../components/pageBody/PageBody'

const HomePage = () => {
    return (
        <div>
            <Header/>
            <BackgroundImg/>
            <PageBody/>

        </div>
    )
}

export default HomePage

